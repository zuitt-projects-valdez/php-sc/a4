<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S4: Activity</title>
</head>
<body>
  <h1>Building</h1>
  <p>
    <?= $building->getName() ?>
  </p>
  <p>
    <?= $building->getFloors() ?>
  </p>
  <p>
    <?= $building->getAddress() ?>
  </p>
  <p>
    <?= $building->setName("Caswynn Building 2") ?>
  </p>

  <h1>Condo</h1>
  <p>
    <?= $condo->getName() ?>
  </p>
  <p>
    <?= $condo->getFloors() ?>
  </p>
  <p>
    <?= $condo->getAddress() ?>
  </p>
  <p>
    <?= $condo->setName("Enzo Condo 2") ?>
  </p>
</body>
</html>