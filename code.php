<?php

class Building
{
  protected $name;
  protected $floors;
  protected $address;

  public function __construct($name, $floors, $address)
  {
    $this->name = $name;
    $this->floors = $floors;
    $this->address = $address;
  }

  public function getName()
  {
    return "The name of this building is $this->name.";
  }
  public function getFloors()
  {
    return "The $this->name has $this->floors floors.";
  }
  public function getAddress()
  {
    return "The $this->name is located at $this->address.";
  }

  public function setName($name)
  {
    $this->name = $name;
    return "The name of this building has been changed to $this->name.";
  }
  public function setFloors($floors)
  {
    return $this->floors = $floors;
  }
  public function setAddress($address)
  {
    return $this->address = $address;
  }
}

class Condominium extends Building
{
  public function getName()
  {
    return "The name of this condominium is $this->name.";
  }
  public function setName($name)
  {
    $this->name = $name;
    return "The name of this condominium has been changed to $name.";
  }
}
/* 
  ALTERNATE SOLUTION
  class Condominium extends Building{} <- this will inherit all the parent methods so can remain empty brackets
*/

$building = new Building("Caswynn Building", 8, "Quezon City, Philippines");

$condo = new Condominium("Enzo Condo", 5, "Makati City, Philippines");
